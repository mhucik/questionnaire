<?php declare(strict_types = 1);

namespace Tests;

use Nette\Configurator;

final class Bootstrap
{
    public static function boot(): Configurator
    {
        $configurator = new Configurator;

        $configurator->setTimeZone('Europe/Prague');
        $configurator->setTempDirectory(__DIR__ . '/../temp/tests');

        $configurator->createRobotLoader()
            ->addDirectory(__DIR__)
            ->register();

        $configurator->addConfig(__DIR__ . '/../app/config/common.neon');
        $configurator->addConfig(__DIR__ . '/../app/config/tests.neon');

        return $configurator;
    }
}
