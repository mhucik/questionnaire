<?php declare(strict_types = 1);

namespace Tests\Integration\Entity;

use App\Entity\Question;
use App\Entity\QuestionOption;
use App\Exception\MissingCorrectOption;
use App\Exception\MissingIncorrectOption;
use App\Exception\QuestionHaveToHaveExactlyOneCorrectOption;
use App\Exception\QuestionNeedToHaveQuestionOptions;
use App\Repository\QuestionRepository;
use Nette\Utils\Random;
use Tests\Integration\IntegrationTestCase;

class QuestionTest extends IntegrationTestCase
{
    private QuestionRepository $questionRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->questionRepository = $this->container->getByType(QuestionRepository::class);
    }


    public function testCreateQuestion(): void
    {
        $questionString = 'Test question';
        $question = Question::createQuestion(
            $questionString,
            QuestionOption::createQuestionOption('test option', FALSE),
            QuestionOption::createQuestionOption('correct test option', TRUE),
        );

        $question = $this->questionRepository->persist($question);
        $this->questionRepository->flush();

        $savedQuestion = $this->questionRepository->getById($question->id);

        $this->assertEquals($savedQuestion->question, $questionString);
        $this->assertCount(2, $savedQuestion->options);
    }


    public function testNoQuestionOptionsAdded(): void
    {
        $this->expectException(QuestionNeedToHaveQuestionOptions::class);

        Question::createQuestion(
            'test question',
        );
    }


    public function testMissingCorrectOption(): void
    {
        $this->expectException(MissingCorrectOption::class);

        Question::createQuestion(
            'test question',
            QuestionOption::createQuestionOption('test option', FALSE),
        );
    }


    public function testCannotHaveMoreCorrectOptions(): void
    {
        $this->expectException(QuestionHaveToHaveExactlyOneCorrectOption::class);

        Question::createQuestion(
            'test question',
            QuestionOption::createQuestionOption('test option', TRUE),
            QuestionOption::createQuestionOption('test option 2', TRUE),
        );
    }


    public function testGetCorrectOption(): void
    {
        $correctOption = QuestionOption::createQuestionOption(Random::generate(), TRUE);
        $incorrectOption = QuestionOption::createQuestionOption(Random::generate(), FALSE);

        $question = Question::createQuestion(
            Random::generate(),
            $correctOption,
            $incorrectOption
        );
        $this->questionRepository->persistAndFlush($question);

        $this->assertEquals($correctOption->getOption(), $question->getCorrectOption()->getOption());
    }


    public function testGetIncorrectOption(): void
    {
        $correctOption = QuestionOption::createQuestionOption(Random::generate(), TRUE);
        $incorrectOption = QuestionOption::createQuestionOption(Random::generate(), FALSE);

        $question = Question::createQuestion(
            Random::generate(),
            $correctOption,
            $incorrectOption
        );
        $this->questionRepository->persistAndFlush($question);

        $this->assertEquals($incorrectOption->getOption(), $question->getIncorrectOption()->getOption());
    }


    public function testGetCorrectOptionWithMissingCorrectOption(): void
    {
        $this->expectException(MissingCorrectOption::class);
        $question = new Question();
        $question->question = Random::generate();

        $this->questionRepository->persistAndFlush($question);

        $question->getCorrectOption();
    }


    public function testGetIncorrectOptionWithMissingIncorrectOption(): void
    {
        $this->expectException(MissingIncorrectOption::class);
        $question = new Question();
        $question->question = Random::generate();

        $this->questionRepository->persistAndFlush($question);

        $question->getIncorrectOption();
    }


    protected function tearDown(): void
    {
        parent::tearDown();
        $questions = $this->questionRepository->findAll();

        foreach ($questions as $question) {
            $this->questionRepository->removeAndFlush($question);
        }
    }
}
