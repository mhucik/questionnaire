<?php declare(strict_types = 1);

namespace Tests\Integration\Repository;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\QuestionOption;
use App\Entity\User;
use App\Repository\AnswerRepository;
use App\Repository\QuestionRepository;
use App\Repository\UserRepository;
use Nette\Utils\Random;
use Tests\Integration\IntegrationTestCase;

final class AnswerRepositoryTest extends IntegrationTestCase
{
    private AnswerRepository $answerRepository;

    private QuestionRepository $questionRepository;

    private User $user;

    private UserRepository $userRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->answerRepository = $this->container->getByType(AnswerRepository::class);
        $this->questionRepository = $this->container->getByType(QuestionRepository::class);
        $this->userRepository = $this->container->getByType(UserRepository::class);
        $this->createRandomQuestions();
        $userRepository = $this->container->getByType(UserRepository::class);
        $this->user = User::createUser();
        $userRepository->persistAndFlush($this->user);
        $this->answerCorrectly($this->user, 3);
        $this->answerIncorrectly($this->user, 2);
    }


    public function testGetAllUserAnswers(): void
    {
        $answers = $this->answerRepository->getAllUserAnswers($this->user);

        $this->assertCount(5, $answers);
    }


    public function testGetCorrectUserAnswers(): void
    {
        $answers = $this->answerRepository->getCorrectUserAnswers($this->user);

        $this->assertCount(3, $answers);
    }


    public function testGetIncorrectUserAnswers(): void
    {
        $answers = $this->answerRepository->getIncorrectUserAnswers($this->user);

        $this->assertCount(2, $answers);
    }


    private function createRandomQuestions(): void
    {
        for ($i = 0; $i < 10; ++$i) {
            $question = Question::createQuestion(
                Random::generate(),
                QuestionOption::createQuestionOption(Random::generate(), FALSE),
                QuestionOption::createQuestionOption(Random::generate(), FALSE),
                QuestionOption::createQuestionOption(Random::generate(), TRUE),
            );

            $this->questionRepository->persist($question);
        }

        $this->questionRepository->flush();
    }


    private function answerCorrectly(User $user, int $count): void
    {
        for ($i = 0; $i < $count;++$i) {
            $question = $this->questionRepository->getRandomQuestionForUser($user);
            $answer = Answer::createAnswer($user, $question->getCorrectOption());
            $this->answerRepository->persist($answer);
        }

        $this->answerRepository->flush();
    }


    private function answerIncorrectly(User $user, int $count): void
    {
        for ($i = 0; $i < $count;++$i) {
            $question = $this->questionRepository->getRandomQuestionForUser($user);
            $answer = Answer::createAnswer($user, $question->getIncorrectOption());
            $this->answerRepository->persist($answer);
        }

        $this->answerRepository->flush();
    }


    protected function tearDown(): void
    {
        parent::tearDown();
        $questions = $this->questionRepository->findAll();
        foreach ($questions as $question) {
            $this->questionRepository->remove($question);
        }

        $this->questionRepository->flush();

        $users = $this->userRepository->findAll();
        foreach ($users as $user) {
            $this->userRepository->remove($user);
        }

        $this->userRepository->flush();
    }


}
