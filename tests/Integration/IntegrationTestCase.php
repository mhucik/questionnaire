<?php declare(strict_types = 1);

namespace Tests\Integration;

use Nette\DI\Container;
use PHPUnit\Framework\TestCase;

abstract class IntegrationTestCase extends TestCase
{
    protected Container $container;

    protected function setUp(): void
    {
        parent::setUp();
        $this->container = \Tests\Bootstrap::boot()->createContainer();
    }

}
