<?php declare(strict_types = 1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return RouteList<Nette\Routing\Route>
     */
    public static function createRouter(): RouteList
    {
        $router = new RouteList;

        $router[] = $admin = new RouteList('Admin');
        $admin->addRoute('admin/<presenter>/<action>[/<id>]', 'Login:default');
        $router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');


        return $router;
    }
}
