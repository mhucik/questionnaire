<?php declare(strict_types = 1);

namespace App\Service;

use App\Entity\Identity;
use App\Repository\UserRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\IIdentity;

final class UserCodeAuthenticator implements IAuthenticator
{
    public const ROLE = 'questionnaire_user';

    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @param array<string> $credentials
     */
    public function authenticate(array $credentials): IIdentity
    {
        [$code] = $credentials;

        $user = $this->userRepository->getBy(['code' => $code]);

        if ( ! $user) {
            throw new AuthenticationException('User not found.');
        }

        return new Identity($user->id, [self::ROLE], $user->getIdentityData());
    }
}
