<?php declare(strict_types = 1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;

final class UserRegistrator
{
    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }


    public function registerNewUser(): User
    {
        $user = User::createUser();

        $this->userRepository->persist($user);
        $this->userRepository->flush();

        return $user;
    }
}
