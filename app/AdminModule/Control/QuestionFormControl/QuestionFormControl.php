<?php declare(strict_types = 1);

namespace App\AdminModule\Control\QuestionFormControl;

use App\Entity\Question;
use App\Entity\QuestionOption;
use App\Form\DynamicForm;
use App\Form\QuestionForm;
use App\Repository\QuestionOptionRepository;
use App\Repository\QuestionRepository;
use Kdyby\Replicator\Container;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Controls\SubmitButton;

final class QuestionFormControl extends Control
{
    private QuestionRepository $questionRepository;

    /**
     * @var array<callable>
     */
    private array $onSuccess = [];

    private QuestionOptionRepository $questionOptionRepository;

    public function __construct(
        QuestionRepository $questionRepository,
        QuestionOptionRepository $questionOptionRepository
    )
    {
        $this->questionRepository = $questionRepository;
        $this->questionOptionRepository = $questionOptionRepository;
    }


    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . '/questionFormControl.latte');
        $this->getTemplate()->render();
    }


    public function setDefault(Question $question): void
    {
        $form = $this->getComponent('form');

        if ( ! $form instanceof Form) {
            return;
        }

        if ($form->isSubmitted()) {
            return;
        }

        /** @var BaseControl $questionId */
        $questionId = $form->getComponent(QuestionForm::QUESTION_ID_FIELD);
        /** @var BaseControl $questionField */
        $questionField = $form->getComponent(QuestionForm::QUESTION_FIELD);

        $questionId->setDefaultValue($question->getId());
        $questionField->setDefaultValue($question->getQuestion());

        /** @var Container $options */
        $options = $form->getComponent(QuestionForm::OPTIONS_FIELD);
        /** @var BaseControl $correctAnswerRadioList */
        $correctAnswerRadioList = $form->getComponent(QuestionForm::CORRECT_ANSWER_FIELD);

        foreach ($question->getOptions() as $i => $option) {
            /** @var \Nette\Forms\Container $questionForm */
            $questionForm = $options->getComponent((string) $i);

            /** @var BaseControl $optionId */
            $optionId = $questionForm->getComponent(QuestionForm::OPTION_ID_FIELD);
            /** @var BaseControl $optionText */
            $optionText = $questionForm->getComponent(QuestionForm::OPTION_OPTION_FIELD);

            $optionId->setDefaultValue($option->getId());
            $optionText->setDefaultValue($option->getOption());

            if ($option->isCorrectOption()) {
                $correctAnswerRadioList->setDefaultValue($i);
            }
        }
    }


    protected function createComponentForm(): DynamicForm
    {
        $form = QuestionForm::create();

        $form->onSuccess[] = function (\Nette\Forms\Form $form, array $values): void {
            /** @var SubmitButton $submitButton */
            $submitButton = $form->getComponent(QuestionForm::SUBMIT_BUTTON);

            if ( ! $submitButton->isSubmittedBy()) {
                return;
            }

            if ($values[QuestionForm::QUESTION_ID_FIELD]) {
                $this->editQuestion($values);
            } else {
                $this->createNewQuestion($values);
            }

            foreach ($this->onSuccess as $onSuccess) {
                ($onSuccess)();
            }
        };

        return $form;
    }


    /**
     * @param array<mixed> $values
     * @throws \App\Exception\MissingCorrectOption
     * @throws \App\Exception\QuestionHaveToHaveExactlyOneCorrectOption
     * @throws \App\Exception\QuestionNeedToHaveQuestionOptions
     */
    private function createNewQuestion(array $values): void
    {
        $options = [];

        foreach ($values[QuestionForm::OPTIONS_FIELD] as $id => $option) {
            $options[] = QuestionOption::createQuestionOption($option[QuestionForm::OPTION_OPTION_FIELD], $id === $values[QuestionForm::CORRECT_ANSWER_FIELD]);
        }

        $questions = Question::createQuestion($values[QuestionForm::QUESTION_FIELD], ...$options);
        $this->questionRepository->persistAndFlush($questions);
    }


    /**
     * @param array<mixed> $values
     */
    private function editQuestion(array $values): void
    {
        $question = $this->questionRepository->getById($values[QuestionForm::QUESTION_ID_FIELD]);

        if ( ! $question) {
            return;
        }

        $question->question = $values[QuestionForm::QUESTION_FIELD];

        $questionOptionsToDelete = array_flip(array_map(static function (QuestionOption $questionOption) {
            return $questionOption->getId();
        }, iterator_to_array($question->getOptions())));

        foreach ($values[QuestionForm::OPTIONS_FIELD] as $i => $option) {
            if ($option[QuestionForm::OPTION_ID_FIELD]) {
                unset($questionOptionsToDelete[$option[QuestionForm::OPTION_ID_FIELD]]);
                $questionOption = $this->questionOptionRepository->getById($option[QuestionForm::OPTION_ID_FIELD]);

                if ( ! $questionOption) {
                    continue;
                }

                $questionOption->option = $option[QuestionForm::OPTION_OPTION_FIELD];
                $questionOption->correctOption = $values[QuestionForm::CORRECT_ANSWER_FIELD] === $i;

                $this->questionOptionRepository->persist($questionOption);
            } else {
                $questionOption = QuestionOption::createQuestionOption($option[QuestionForm::OPTION_OPTION_FIELD], $i === $values[QuestionForm::CORRECT_ANSWER_FIELD]);
                $questionOption->question = $question;

                $this->questionOptionRepository->persist($questionOption);
            }
        }

        foreach ($questionOptionsToDelete as $id => $questionOptionToDelete) {
            $questionOption = $this->questionOptionRepository->getById($id);

            if ( ! $questionOption) {
                continue;
            }

            $this->questionOptionRepository->remove($questionOption);
        }

        $this->questionOptionRepository->flush();
        $this->questionRepository->flush();
    }


    public function addOnSuccess(callable $callback): void
    {
        $this->onSuccess[] = $callback;
    }
}
