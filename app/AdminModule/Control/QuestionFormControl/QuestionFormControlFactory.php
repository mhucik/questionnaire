<?php declare(strict_types = 1);

namespace App\AdminModule\Control\QuestionFormControl;

interface QuestionFormControlFactory
{
    public function create(): QuestionFormControl;
}
