<?php declare(strict_types = 1);

namespace App\AdminModule\Control\AdminUserLoginFormControl;

interface AdminUserLoginFormControlFactory
{
    public function create(): AdminUserLoginFormControl;
}
