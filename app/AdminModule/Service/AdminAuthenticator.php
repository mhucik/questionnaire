<?php declare(strict_types = 1);

namespace App\AdminModule\Service;

use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

final class AdminAuthenticator implements IAuthenticator
{
    public const ROLE = 'editor';

    private string $loginUsername;

    private string $loginPassword;

    public function __construct(
        string $loginUsername,
        string $loginPassword
    )
    {
        $this->loginUsername = $loginUsername;
        $this->loginPassword = $loginPassword;
    }


    /**
     * @param array<string> $credentials
     */
    function authenticate(array $credentials): IIdentity
    {
        [$username, $password] = $credentials;

        if ($username !== $this->loginUsername) {
            throw new AuthenticationException('Invalid credentials.');
        }

        if ($password !== $this->loginPassword) {
            throw new AuthenticationException('Invalid credentials.');
        }

        return new Identity(1, [self::ROLE]);
    }

}
