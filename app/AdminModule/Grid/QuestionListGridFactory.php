<?php declare(strict_types = 1);

namespace App\AdminModule\Grid;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use Ublaboo\DataGrid\DataGrid;

final class QuestionListGridFactory
{
    private QuestionRepository $questionRepository;

    public function __construct(
        QuestionRepository $questionRepository
    )
    {
        $this->questionRepository = $questionRepository;
    }


    public function create(
        string $addQuestionLinkString,
        string $deleteQuestionLinkString
    ): DataGrid
    {
        $grid = new DataGrid();

        $grid->setDataSource($this->questionRepository->findAll());

        $grid->addColumnText('question', 'Question');
        $grid->addColumnNumber('noOptions', 'Number of options')
            ->setRenderer(static function (Question $question): int {
                return $question->getOptions()->count();
            })
        ;
        $grid->addAction('edit', 'Edit', 'EditQuestion:');
        $grid->addAction('delete', 'Delete', $deleteQuestionLinkString);

        $grid->addToolbarButton($addQuestionLinkString, 'Add question');

        return $grid;
    }
}
