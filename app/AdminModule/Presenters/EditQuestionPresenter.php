<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Control\QuestionFormControl\QuestionFormControl;
use App\AdminModule\Control\QuestionFormControl\QuestionFormControlFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Repository\QuestionRepository;

final class EditQuestionPresenter extends SecuredPresenter
{
    private QuestionRepository $questionRepository;

    private QuestionFormControlFactory $questionFormControlFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        QuestionRepository $questionRepository,
        QuestionFormControlFactory $questionFormControlFactory
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->questionRepository = $questionRepository;
        $this->questionFormControlFactory = $questionFormControlFactory;
    }


    public function actionDefault(int $id): void
    {
        $question = $this->questionRepository->getById($id);

        if ( ! $question) {
            $this->error();
        }

        /** @var QuestionFormControl $questionForm */
        $questionForm = $this->getComponent('questionForm');
        $questionForm->setDefault($question);
    }


    protected function createComponentQuestionForm(): QuestionFormControl
    {
        $control = $this->questionFormControlFactory->create();

        $control->addOnSuccess(function (): void {
            $this->flashMessage('Question successfully saved');
            $this->redirect('QuestionList:');
        });

        return $control;
    }
}
