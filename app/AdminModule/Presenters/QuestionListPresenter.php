<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Grid\QuestionListGridFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Repository\QuestionRepository;
use Ublaboo\DataGrid\DataGrid;

final class QuestionListPresenter extends SecuredPresenter
{
    private QuestionListGridFactory $questionListGridFactory;

    private QuestionRepository $questionRepository;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        QuestionListGridFactory $questionListGridFactory,
        QuestionRepository $questionRepository
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->questionListGridFactory = $questionListGridFactory;
        $this->questionRepository = $questionRepository;
    }


    public function createComponentQuestionList(): DataGrid
    {
        return $this->questionListGridFactory->create('AddQuestion:', 'deleteQuestion!');
    }


    public function handleDeleteQuestion(int $id): void
    {
        $question = $this->questionRepository->getById($id);

        if ( ! $question) {
            $this->flashMessage('Cannot delete. Question not exists anymore.', 'danger');
            $this->redirect('this');
        }

        $this->questionRepository->removeAndFlush($question);

        $this->flashMessage('Question was successfully deleted.');
        $this->redirect('this');
    }
}
