<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Service\AdminAuthenticator;
use App\Presenters\BasePresenter;

abstract class SecuredPresenter extends BasePresenter
{
    /**
     * @param mixed $element
     */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);

        if ( ! $this->user->isLoggedIn()) {
            $this->flashMessage('Please, login as correct user.', 'danger');
            $this->redirect('Login:');
        }

        if ( ! $this->user->isInRole(AdminAuthenticator::ROLE)) {
            $this->error();
        }
    }


    public function handleLogout(): void
    {
        $this->user->logout(TRUE);

        $this->flashMessage('You have been successfully logout.');
        $this->redirect(':Homepage:');
    }
}
