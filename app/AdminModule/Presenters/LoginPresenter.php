<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Control\AdminUserLoginFormControl\AdminUserLoginFormControl;
use App\AdminModule\Control\AdminUserLoginFormControl\AdminUserLoginFormControlFactory;
use App\AdminModule\Service\AdminAuthenticator;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Presenters\BasePresenter;

final class LoginPresenter extends BasePresenter
{
    private AdminUserLoginFormControlFactory $adminUserLoginFormFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        AdminUserLoginFormControlFactory $adminUserLoginFormFactory
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->adminUserLoginFormFactory = $adminUserLoginFormFactory;
    }


    /**
     * @param mixed $element
     */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);
        if ($this->user->isLoggedIn() && $this->user->isInRole(AdminAuthenticator::ROLE)) {
            $this->redirect('QuestionList:');
        }
    }


    protected function createComponentLoginForm(): AdminUserLoginFormControl
    {
        $control = $this->adminUserLoginFormFactory->create();

        $control->addOnSuccess(function (): void {
            $this->flashMessage('Login successful', 'success');
            $this->redirect('QuestionList:');
        });

        $control->addOnError(function (): void {
            $this->flashMessage('Login unsuccessful', 'danger');
        });

        return $control;
    }
}
