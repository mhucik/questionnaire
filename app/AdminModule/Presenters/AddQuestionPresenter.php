<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Control\QuestionFormControl\QuestionFormControl;
use App\AdminModule\Control\QuestionFormControl\QuestionFormControlFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;

final class AddQuestionPresenter extends SecuredPresenter
{
    private QuestionFormControlFactory $addQuestionFormControlFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        QuestionFormControlFactory $addQuestionFormControlFactory
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->addQuestionFormControlFactory = $addQuestionFormControlFactory;
    }


    protected function createComponentAddQuestionForm(): QuestionFormControl
    {
        $control = $this->addQuestionFormControlFactory->create();

        $control->addOnSuccess(function (): void {
            $this->flashMessage('Question successfully saved');
            $this->redirect('QuestionList:');
        });

        return $control;
    }
}
