<?php declare(strict_types = 1);

namespace App\Form;

use Kdyby\Replicator\Container;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;

final class QuestionForm
{
    public const QUESTION_FIELD = 'question';
    public const CORRECT_ANSWER_FIELD = 'correctAnswer';
    public const OPTIONS_FIELD = 'options';
    public const QUESTION_ID_FIELD = 'id';
    public const OPTION_ID_FIELD = 'id';
    public const OPTION_OPTION_FIELD = 'option';
    public const SUBMIT_BUTTON = 'save';

    public static function create(): DynamicForm
    {
        $form = new DynamicForm();

        $form->addHidden(self::QUESTION_ID_FIELD);

        $form
            ->addText(self::QUESTION_FIELD, 'Question')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired()
        ;

        $list = $form
            ->addRadioList(self::CORRECT_ANSWER_FIELD, 'Is correct')
            ->setRequired()
        ;

        $options = $form->addDynamic(self::OPTIONS_FIELD, static function (\Nette\Forms\Container $options) use ($list): void {
            $options->addHidden(self::OPTION_ID_FIELD);

            $options
                ->addText(self::OPTION_OPTION_FIELD, 'Answer option')
                ->setHtmlAttribute('class', 'form-control')
                ->setRequired()
            ;

            $items = $list->getItems();
            $keys = array_keys($items);
            $lastKey = count($items) ? end($keys) : -1;
            $newKey = ++$lastKey;
            $items[$newKey] = $newKey;

            $list->setItems($items);

            $options->addSubmit('removeOption', '-')
                ->setValidationScope([])
                ->setHtmlAttribute('class', 'btn btn-danger')
                ->onClick[] = static function (SubmitButton $button): void {
                    /** @var Control $parentContainer */
                    $parentContainer = $button->getParent();

                    /** @var Container $container */
                    $container = $parentContainer->getParent();

                    $container->remove($parentContainer, TRUE);
                }
            ;
        }, 1);

        $options
            ->addSubmit('addOption', 'Add option')
            ->setValidationScope([])
            ->setHtmlAttribute('class', 'btn btn-success')
            ->onClick[] = static function (SubmitButton $button): void {
                /** @var Container $container */
                $container = $button->getParent();

                $container->createOne();
            }
        ;

        $form
            ->addSubmit(self::SUBMIT_BUTTON, 'Save')
            ->setHtmlAttribute('class', 'btn btn-success')
        ;

        return $form;
    }
}
