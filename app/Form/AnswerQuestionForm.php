<?php declare(strict_types = 1);

namespace App\Form;

use App\Entity\Question;
use App\Entity\QuestionOption;
use Nette\Application\UI\Form;

final class AnswerQuestionForm
{
    public const QUESTION_OPTION_FIELD = 'questionOption';
    public const QUESTION_ID_FIELD = 'questionId';

    public static function create(Question $question): Form
    {
        $form = new Form();

        $form->addHidden(self::QUESTION_ID_FIELD, (string) $question->getId());

        $optionsList = [];

        /** @var QuestionOption $option */
        foreach ($question->getOptions() as $option) {
            $optionsList[$option->getId()] = $option->getOption();
        }

        $form
            ->addRadioList(self::QUESTION_OPTION_FIELD, 'Choose one correct option', $optionsList)
        ;

        $form
            ->addSubmit('save', 'Answer')
            ->setHtmlAttribute('class', 'btn btn-success')
        ;

        return $form;
    }
}
