<?php declare(strict_types = 1);

namespace App\Form;

use Kdyby\Replicator\Container;
use Nette\Application\UI\Form;

/**
 * @method Container addDynamic(string $name, callable $factory, int $createDefault = 0, bool $forceDefault = FALSE)
 */
final class DynamicForm extends Form
{

}
