<?php declare(strict_types = 1);

namespace App\Control\LoginUserForm;

interface LoginUserFormFactory
{
    public function create(): LoginUserForm;
}
