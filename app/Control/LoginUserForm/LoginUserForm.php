<?php declare(strict_types = 1);

namespace App\Control\LoginUserForm;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;

final class LoginUserForm extends Control
{
    /**
     * @var array<callable>
     */
    private array $onSuccess = [];

    /**
     * @var array<callable>
     */
    private array $onError = [];

    private User $user;

    public function __construct(
        User $user
    )
    {
        $this->user = $user;
    }


    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . '/loginUserForm.latte');
        $this->getTemplate()->render();
    }


    protected function createComponentForm(): Form
    {
        $form = new Form();

        $form
            ->addText('code', 'User code')
            ->setHtmlAttribute('class', 'form-control')
        ;
        $form
            ->addSubmit('submit', 'Login')
            ->setHtmlAttribute('class', 'btn btn-primary btn-block')
        ;

        $form->onSuccess[] = function (\Nette\Forms\Form $form): void {
            /** @var array<string, mixed> $values */
            $values = $form->getValues();

            try {
                $this->user->login($values['code']);
            } catch (AuthenticationException $e) {
                foreach ($this->onError as $onError) {
                    ($onError)();
                }

                return;
            }

            foreach ($this->onSuccess as $onSuccess) {
                ($onSuccess)();
            }
        };

        return $form;
    }


    public function addOnError(callable $callable): void
    {
        $this->onError[] = $callable;
    }


    public function addOnSuccess(callable $callable): void
    {
        $this->onSuccess[] = $callable;
    }
}
