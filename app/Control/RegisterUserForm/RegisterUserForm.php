<?php declare(strict_types = 1);

namespace App\Control\RegisterUserForm;

use App\Service\UserRegistrator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

final class RegisterUserForm extends Control
{
    private UserRegistrator $userRegistrator;

    /**
     * @var array<callable>
     */
    private array $onSuccess = [];

    public function __construct(
        UserRegistrator $userRegistrator
    )
    {
        $this->userRegistrator = $userRegistrator;
    }


    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . "/registerUserForm.latte");
        $this->getTemplate()->render();
    }


    public function createComponentForm(): Form
    {
        $form = new Form();

        $form
            ->addSubmit('sendRequest', 'Register to questionnaire')
            ->setHtmlAttribute('class', 'btn btn-success btn-block')
        ;

        $form->onSuccess[] = function (): void {
            $user = $this->userRegistrator->registerNewUser();

            foreach ($this->onSuccess as $onSuccess) {
                ($onSuccess)($user);
            }
        };

        return $form;
    }


    public function addOnSuccess(callable $callable): void
    {
        $this->onSuccess[] = $callable;
    }
}
