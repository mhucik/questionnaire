<?php declare(strict_types = 1);

namespace App\Control\RegisterUserForm;

interface RegisterUserFormFactory
{
    public function create(): RegisterUserForm;
}
