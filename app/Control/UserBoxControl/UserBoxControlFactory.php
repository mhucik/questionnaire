<?php declare(strict_types = 1);

namespace App\Control\UserBoxControl;

use Nette\Security\User;

interface UserBoxControlFactory
{
    public function create(User $user): UserBoxControl;
}
