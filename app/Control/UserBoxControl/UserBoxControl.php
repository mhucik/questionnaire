<?php declare(strict_types = 1);

namespace App\Control\UserBoxControl;

use App\AdminModule\Service\AdminAuthenticator;
use Nette\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Security\User;

/**
 * @method Template getTemplate()
 */
final class UserBoxControl extends Control
{
    private User $user;

    /**
     * @var array<callable>
     */
    private array $onLogout = [];

    public function __construct(
        User $user
    )
    {
        $this->user = $user;
    }


    public function render(): void
    {
        if ( ! $this->user->isLoggedIn()) {
            return;
        }

        $user = $this->user->getIdentity();

        $this->getTemplate()->add('isEditor', $this->user->isInRole(AdminAuthenticator::ROLE));
        $this->getTemplate()->add('appUser', $user);

        $this->getTemplate()->setFile(__DIR__ . '/userBoxControl.latte');
        $this->getTemplate()->render();
    }


    public function handleLogout(): void
    {
        $this->user->logout(TRUE);

        foreach ($this->onLogout as $onLogout) {
            ($onLogout)();
        }
    }


    public function addOnLogout(callable $onLogout): void
    {
        $this->onLogout[] = $onLogout;
    }
}
