<?php declare(strict_types = 1);

namespace App\Control\UserAnsweredQuestionsListControl;

use App\Entity\QuestionOption;
use App\Entity\User;
use App\Repository\AnswerRepository;
use Nette\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\Template;

/**
 * @method Template getTemplate()
 */
final class UserAnsweredQuestionsListControl extends Control
{
    private User $user;

    private AnswerRepository $answerRepository;

    public function __construct(
        User $user,
        AnswerRepository $answerRepository
    )
    {
        $this->user = $user;
        $this->answerRepository = $answerRepository;
    }


    public function render(): void
    {
        $this->getTemplate()->add('userAnswers', $this->answerRepository->getAllUserAnswers($this->user));
        $this->getTemplate()->addFilter('questionOptionClass', static function (QuestionOption $questionOption, QuestionOption $choosenOption): string {
            if ($questionOption->isCorrectOption()) {
                return 'list-group-item-success';
            }

            if ( ! $choosenOption->isCorrectOption() && $questionOption->getId() === $choosenOption->getId()) {
                return 'list-group-item-danger';
            }

            return '';
        });

        $this->getTemplate()->setFile(__DIR__ . '/userAnsweredQuestionsListControl.latte');
        $this->getTemplate()->render();
    }
}
