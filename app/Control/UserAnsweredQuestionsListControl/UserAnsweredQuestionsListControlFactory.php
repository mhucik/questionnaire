<?php declare(strict_types = 1);

namespace App\Control\UserAnsweredQuestionsListControl;

use App\Entity\User;

interface UserAnsweredQuestionsListControlFactory
{
    public function create(User $user): UserAnsweredQuestionsListControl;
}
