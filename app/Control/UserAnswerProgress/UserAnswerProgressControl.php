<?php declare(strict_types = 1);

namespace App\Control\UserAnswerProgress;

use App\Entity\User;
use App\Repository\AnswerRepository;
use Nette\Application\UI\Control;
use Nette\Bridges\ApplicationLatte\Template;

/**
 * @method Template getTemplate()
 */
final class UserAnswerProgressControl extends Control
{
    private User $user;

    private AnswerRepository $answerRepository;

    public function __construct(
        User $user,
        AnswerRepository $answerRepository
    )
    {
        $this->user = $user;
        $this->answerRepository = $answerRepository;
    }


    public function render(): void
    {
        $allUserAnswers = $this->answerRepository->getAllUserAnswers($this->user);

        $this->getTemplate()->add('answeredQuestions', $allUserAnswers);

        $this->getTemplate()->setFile(__DIR__ . '/userAnswerProgressControl.latte');
        $this->getTemplate()->render();
    }
}
