<?php declare(strict_types = 1);

namespace App\Control\UserAnswerProgress;

use App\Entity\User;

interface UserAnswerProgressControlFactory
{
    public function create(User $user): UserAnswerProgressControl;
}
