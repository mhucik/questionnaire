<?php declare(strict_types = 1);

namespace App\Control\UserQuestionFormControl;

use App\Entity\Question;
use App\Entity\User;

interface UserQuestionFormControlFactory
{
    public function create(User $user, Question $userQuestion): UserQuestionFormControl;
}
