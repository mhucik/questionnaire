<?php declare(strict_types = 1);

namespace App\Control\UserQuestionFormControl;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\QuestionOption;
use App\Entity\User;
use App\Form\AnswerQuestionForm;
use App\Repository\AnswerRepository;
use App\Repository\QuestionOptionRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Bridges\ApplicationLatte\Template;

/**
 * @method Template getTemplate()
 */
final class UserQuestionFormControl extends Control
{
    private User $user;

    private Question $userQuestion;

    private QuestionOptionRepository $questionOptionRepository;

    private AnswerRepository $answerRepository;

    /**
     * @var array<callable>
     */
    private array $onSuccess = [];

    public function __construct(
        User $user,
        Question $userQuestion,
        QuestionOptionRepository $questionOptionRepository,
        AnswerRepository $answerRepository
    )
    {
        $this->user = $user;
        $this->questionOptionRepository = $questionOptionRepository;
        $this->answerRepository = $answerRepository;
        $this->userQuestion = $userQuestion;
    }


    public function render(): void
    {
        $this->getTemplate()->add('question', $this->userQuestion->getQuestion());
        $this->getTemplate()->setFile(__DIR__ . '/userQuestionFormControl.latte');
        $this->getTemplate()->render();
    }


    protected function createComponentForm(): Form
    {
        $form = AnswerQuestionForm::create($this->userQuestion);

        $form->onSuccess[] = function (\Nette\Forms\Form $form): void {
            /** @var array<mixed> $values */
            $values = $form->getValues();

            /** @var QuestionOption $choosenOption */
            $choosenOption = $this->questionOptionRepository->getById($values[AnswerQuestionForm::QUESTION_OPTION_FIELD]);
            $answer = Answer::createAnswer($this->user, $choosenOption);

            $this->answerRepository->persistAndFlush($answer);

            foreach ($this->onSuccess as $success) {
                ($success)($choosenOption->isCorrectOption());
            }
        };

        return $form;
    }


    public function addOnSuccess(callable $onSuccess): void
    {
        $this->onSuccess[] = $onSuccess;
    }
}
