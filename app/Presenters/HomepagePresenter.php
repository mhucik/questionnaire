<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Control\LoginUserForm\LoginUserForm;
use App\Control\LoginUserForm\LoginUserFormFactory;
use App\Control\RegisterUserForm\RegisterUserForm;
use App\Control\RegisterUserForm\RegisterUserFormFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Entity\User;
use App\Service\UserCodeAuthenticator;
use Nette;

final class HomepagePresenter extends BasePresenter
{
    private RegisterUserFormFactory $registerUserFormFactory;

    private LoginUserFormFactory $loginUserFormFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        RegisterUserFormFactory $registerUserFormFactory,
        LoginUserFormFactory $loginUserFormFactory
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->registerUserFormFactory = $registerUserFormFactory;
        $this->loginUserFormFactory = $loginUserFormFactory;
    }


    /**
     * @param mixed $element
     * @throws Nette\Application\AbortException
     * @throws Nette\Application\ForbiddenRequestException
     */
    public function checkRequirements($element): void
    {
        parent::checkRequirements($element);

        if ($this->user->isLoggedIn() && $this->user->isInRole(UserCodeAuthenticator::ROLE)) {
            $this->redirect('Questionnaire:');
        }
    }


    protected function createComponentRegisterUser(): RegisterUserForm
    {
        $control = $this->registerUserFormFactory->create();

        $control->addOnSuccess(function (User $user): void {
            $this->flashMessage(\sprintf('Registration completed, login with "%s" code.', $user->getCode()), 'success');
            $this->redirect('this');
        });

        return $control;
    }


    protected function createComponentLoginUser(): LoginUserForm
    {
        $control = $this->loginUserFormFactory->create();

        $control->addOnSuccess(function (): void {
            $this->flashMessage('Login successful', 'success');
            $this->redirect('Questionnaire:');
        });

        $control->addOnError(function (): void {
            $this->flashMessage('Login unsuccessful', 'danger');
            $this->redirect('this');
        });

        return $control;
    }
}
