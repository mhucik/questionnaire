<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Control\UserAnsweredQuestionsListControl\UserAnsweredQuestionsListControl;
use App\Control\UserAnsweredQuestionsListControl\UserAnsweredQuestionsListControlFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Repository\UserRepository;

final class AnsweredQuestionsListPresenter extends SecuredPresenter
{
    private UserAnsweredQuestionsListControlFactory $userAnsweredQuestionsListControlFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        UserRepository $userRepository,
        UserAnsweredQuestionsListControlFactory $userAnsweredQuestionsListControlFactory
    )
    {
        parent::__construct(
            $userBoxControlFactory,
            $userRepository
        );
        $this->userAnsweredQuestionsListControlFactory = $userAnsweredQuestionsListControlFactory;
    }


    protected function createComponentUserQuestions(): UserAnsweredQuestionsListControl
    {
        return $this->userAnsweredQuestionsListControlFactory->create($this->getQuestionnaireUser());
    }
}
