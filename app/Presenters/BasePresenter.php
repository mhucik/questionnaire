<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Control\UserBoxControl\UserBoxControl;
use App\Control\UserBoxControl\UserBoxControlFactory;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
    private UserBoxControlFactory $userBoxControlFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory
    )
    {
        parent::__construct();
        $this->userBoxControlFactory = $userBoxControlFactory;
    }


    protected function createComponentUserBox(): UserBoxControl
    {
        $control = $this->userBoxControlFactory->create($this->getUser());

        $control->addOnLogout(function (): void {
            $this->flashMessage('You have been successfully logout.', 'success');
            $this->redirect(':Homepage:');
        });

        return $control;
    }
}
