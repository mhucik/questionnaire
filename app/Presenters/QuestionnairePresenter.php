<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Control\UserAnswerProgress\UserAnswerProgressControl;
use App\Control\UserAnswerProgress\UserAnswerProgressControlFactory;
use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Control\UserQuestionFormControl\UserQuestionFormControl;
use App\Control\UserQuestionFormControl\UserQuestionFormControlFactory;
use App\Entity\Question;
use App\Exception\NoQuestionsAvailableForUser;
use App\Exception\UserAnsweredOnMaximumQuestions;
use App\Repository\QuestionRepository;
use App\Repository\UserRepository;

final class QuestionnairePresenter extends SecuredPresenter
{
    private UserQuestionFormControlFactory $userQuestionFormControlFactory;

    private QuestionRepository $questionRepository;

    private Question $userQuestion;

    private UserAnswerProgressControlFactory $userAnswerProgressControlFactory;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        UserQuestionFormControlFactory $userQuestionFormControlFactory,
        UserRepository $userRepository,
        QuestionRepository $questionRepository,
        UserAnswerProgressControlFactory $userAnswerProgressControlFactory
    )
    {
        parent::__construct(
            $userBoxControlFactory,
            $userRepository
        );
        $this->userQuestionFormControlFactory = $userQuestionFormControlFactory;
        $this->questionRepository = $questionRepository;
        $this->userAnswerProgressControlFactory = $userAnswerProgressControlFactory;
    }


    public function actionDefault(): void
    {
        try {
            $this->userQuestion = $this->getUserQuestion();
        } catch (NoQuestionsAvailableForUser $e) {
            $this->setView('noQuestionAvailableForUser');
        } catch (UserAnsweredOnMaximumQuestions $e) {
            $this->redirect('AnsweredQuestionsList:');
        }
    }


    protected function createComponentUserQuestionForm(): UserQuestionFormControl
    {
        $control = $this->userQuestionFormControlFactory->create($this->getQuestionnaireUser(), $this->userQuestion);

        $control->addOnSuccess(function (bool $isCorrectAnswer) {
            $identity = $this->getIdentity();
            $identity->questionAnswered();

            $this->flashMessage(\sprintf('Your answer was %s', $isCorrectAnswer ? 'correct' : 'wrong'), $isCorrectAnswer ? 'success' : 'danger');
            $this->redirect('this');
        });

        return $control;
    }


    private function getUserQuestion(): Question
    {
        $questionId = $this->getIdentity()->getQuestionId();

        if ( ! $questionId) {
            $newQuestion = $this->questionRepository->getRandomQuestionForUser($this->getQuestionnaireUser());
            $this->getIdentity()->setQuestion($newQuestion->getId());

            return $newQuestion;
        }

        $generatedQuestion = $this->questionRepository->getById($questionId);

        if ( ! $generatedQuestion) {
            $this->flashMessage('Something got wrong, please try to answer again.');
            $this->redirect('this');
        }

        return $generatedQuestion;
    }


    protected function createComponentUserProgressBar(): UserAnswerProgressControl
    {
        return $this->userAnswerProgressControlFactory->create($this->getQuestionnaireUser());
    }
}
