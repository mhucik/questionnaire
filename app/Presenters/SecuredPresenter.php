<?php declare(strict_types = 1);

namespace App\Presenters;

use App\Control\UserBoxControl\UserBoxControlFactory;
use App\Entity\Identity;
use App\Entity\User;
use App\Repository\UserRepository;

abstract class SecuredPresenter extends BasePresenter
{
    private User $questionnaireUser;

    private UserRepository $userRepository;

    public function __construct(
        UserBoxControlFactory $userBoxControlFactory,
        UserRepository $userRepository
    )
    {
        parent::__construct($userBoxControlFactory);
        $this->userRepository = $userRepository;
    }


    /**
     * @param mixed $element
     * @throws \Nette\Application\BadRequestException
     */
    public function checkRequirements($element): void
    {
        if ( ! $this->user->isLoggedIn()) {
            $this->flashMessage('Please, login as correct user.', 'danger');
            $this->redirect('Homepage:');
        }

        /** @var \Nette\Security\Identity $identity */
        $identity = $this->user->getIdentity();
        $userId = $identity->getId();

        $questionnaireUser = $this->userRepository->getById($userId);

        if ( ! $questionnaireUser) {
            $this->error();
        }

        $this->questionnaireUser = $questionnaireUser;
    }


    public function getQuestionnaireUser(): User
    {
        return $this->questionnaireUser;
    }


    public function getIdentity(): Identity
    {
        $identity = $this->user->getIdentity();

        if ( ! $identity instanceof Identity) {
            $this->error('Incorrectly authenticated user.');
        }

        return $identity;
    }
}
