<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Answer;
use App\Entity\User;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Repository\Repository;

/**
 * @method ICollection|Answer[] findBy(array $conds)
 */
final class AnswerRepository extends Repository
{
    public static function getEntityClassNames(): array
    {
        return [Answer::class];
    }


    /**
     * @return Answer[]
     */
    public function getAllUserAnswers(User $user): array
    {
        /** @var Answer[] $answers */
        $answers = $this->findBy([
            'user' => $user->getId(),
        ])->fetchAll();

        return $answers;
    }


    /**
     * @return Answer[]
     */
    public function getCorrectUserAnswers(User $user): array
    {
        /** @var Answer[] $answers */
        $answers = $this->findBy([
            'user' => $user->getId(),
            'this->choosenOption->correctOption' => TRUE,
        ])->fetchAll();

        return $answers;
    }


    /**
     * @return Answer[]
     */
    public function getIncorrectUserAnswers(User $user): array
    {
        /** @var Answer[] $answers */
        $answers = $this->findBy([
            'user' => $user->getId(),
            'this->choosenOption->correctOption' => FALSE,
        ])->fetchAll();

        return $answers;
    }
}
