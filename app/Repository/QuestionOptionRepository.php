<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\QuestionOption;
use Nextras\Orm\Repository\Repository;

/**
 * @method QuestionOption|NULL getById(int $id)
 */
final class QuestionOptionRepository extends Repository
{
    public static function getEntityClassNames(): array
    {
        return [QuestionOption::class];
    }
}
