<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\User;
use Nextras\Orm\Repository\Repository;

/**
 * @method NULL|User getBy(array $conds)
 * @method NULL|User getById($id)
 */
final class UserRepository extends Repository
{
    public static function getEntityClassNames(): array
    {
        return [User::class];
    }
}
