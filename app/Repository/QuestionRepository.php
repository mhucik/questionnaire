<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Question;
use App\Entity\User;
use App\Mapper\QuestionMapper;
use Nextras\Orm\Repository\Repository;

/**
 * @method Question|NULL getBy(array $conds)
 * @method Question|NULL getById(int $id)
 */
class QuestionRepository extends Repository
{
    public static function getEntityClassNames(): array
    {
        return [Question::class];
    }


    public function getRandomQuestionForUser(User $user): Question
    {
        /** @var QuestionMapper $mapper */
        $mapper = $this->mapper;

        return $mapper->getRandomQuestionForUser($user);
    }
}
