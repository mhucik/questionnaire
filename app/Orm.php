<?php declare(strict_types = 1);

namespace App;

use App\Repository\AnswerRepository;
use App\Repository\QuestionOptionRepository;
use App\Repository\QuestionRepository;
use App\Repository\UserRepository;

/**
 * @property-read QuestionOptionRepository $questionOptions
 * @property-read QuestionRepository $questions
 * @property-read AnswerRepository $answers
 * @property-read UserRepository $users
 */
class Orm extends \Nextras\Orm\Model\Model
{
}
