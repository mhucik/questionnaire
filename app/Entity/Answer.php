<?php declare(strict_types = 1);

namespace App\Entity;

use Nextras\Orm\Entity\Entity;

/**
 * @property int $id {primary}
 * @property User $user {m:1 User::$answers}
 * @property QuestionOption $choosenOption {m:1 QuestionOption, oneSided=true}
 */
final class Answer extends Entity
{
    public static function createAnswer(
        User $user,
        QuestionOption $choosenOption
    ): self
    {
        $answer = new self();

        $answer->user = $user;
        $answer->choosenOption = $choosenOption;

        return $answer;
    }


    public function getChoosenOption(): QuestionOption
    {
        return $this->choosenOption;
    }
}
