<?php declare(strict_types = 1);

namespace App\Entity;

final class Identity extends \Nette\Security\Identity
{
    private ?int $questionId = NULL;

    public function setQuestion(int $questionId): void
    {
        $this->questionId = $questionId;
    }


    public function questionAnswered(): void
    {
        $this->questionId = NULL;
    }


    public function getQuestionId(): ?int
    {
        return $this->questionId;
    }
}
