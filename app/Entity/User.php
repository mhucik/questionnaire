<?php declare(strict_types = 1);

namespace App\Entity;

use Nette\Utils\Random;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int $id {primary}
 * @property string $code
 * @property OneHasMany|Answer[] $answers {1:m Answer::$user}
 */
final class User extends Entity
{
    public static function createUser(): User
    {
        $user = new self();

        $user->code = Random::generate(6);

        return $user;
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function getCode(): string
    {
        return $this->code;
    }


    /**
     * @return array<string, int|string>
     */
    public function getIdentityData(): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
        ];
    }
}
