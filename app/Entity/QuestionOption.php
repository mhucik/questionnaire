<?php declare(strict_types = 1);

namespace App\Entity;

use Nextras\Orm\Entity\Entity;

/**
 * @property int $id {primary}
 * @property string $option
 * @property bool $correctOption {default FALSE}
 * @property Question $question {m:1 Question::$options}
 */
final class QuestionOption extends Entity
{
    public static function createQuestionOption(
        string $option,
        bool $isCorrectOption
    ): self
    {
        $questionOption = new self();

        $questionOption->option = $option;
        $questionOption->correctOption = $isCorrectOption;

        return $questionOption;
    }


    public function isCorrectOption(): bool
    {
        return $this->correctOption;
    }


    public function getOption(): string
    {
        return $this->option;
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function getQuestion(): Question
    {
        return $this->question;
    }
}
