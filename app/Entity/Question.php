<?php declare(strict_types = 1);

namespace App\Entity;

use App\Exception\MissingCorrectOption;
use App\Exception\MissingIncorrectOption;
use App\Exception\QuestionHaveToHaveExactlyOneCorrectOption;
use App\Exception\QuestionNeedToHaveQuestionOptions;
use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int $id {primary}
 * @property string $question
 * @property OneHasMany|QuestionOption[] $options {1:m QuestionOption::$question, cascade=[persist, remove]}
 */
final class Question extends Entity
{
    public static function createQuestion(
        string $questionString,
        QuestionOption ... $questionOptions
    ): self
    {
        if ( ! $questionOptions) {
            throw new QuestionNeedToHaveQuestionOptions();
        }

        $question = new self();

        $question->question = $questionString;

        $correctOptionsCounter = 0;
        foreach ($questionOptions as $questionOption) {
            if ($questionOption->correctOption) {
                ++$correctOptionsCounter;
            }

            $question->addOption($questionOption);
        }

        if ($correctOptionsCounter === 0) {
            throw new MissingCorrectOption();
        }

        if ($correctOptionsCounter > 1) {
            throw new QuestionHaveToHaveExactlyOneCorrectOption();
        }

        return $question;
    }


    public function addOption(QuestionOption $questionOption): self
    {
        $this->options->add($questionOption);

        return $this;
    }


    /**
     * @return OneHasMany|QuestionOption[]
     */
    public function getOptions(): OneHasMany
    {
        return $this->options;
    }


    public function getCorrectOption(): QuestionOption
    {
        /** @var QuestionOption $option */
        foreach ($this->getOptions() as $option) {
            if ($option->isCorrectOption()) {
                return $option;
            }
        }

        throw new MissingCorrectOption();
    }


    public function getIncorrectOption(): QuestionOption
    {
        foreach ($this->getOptions() as $option) {
            if ( ! $option->isCorrectOption()) {
                return $option;
            }
        }

        throw new MissingIncorrectOption();
    }


    public function getQuestion(): string
    {
        return $this->question;
    }


    public function getId(): int
    {
        return $this->id;
    }
}
