<?php declare(strict_types = 1);

namespace App\Mapper;

use App\Entity\Question;
use App\Entity\User;
use App\Exception\NoQuestionsAvailableForUser;
use App\Exception\UserAnsweredOnMaximumQuestions;
use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Mapper\Mapper;

final class QuestionMapper extends Mapper
{
    public const MAX_USER_ANSWERS = 10;

    public function getTableName() : string{
        return 'question';
    }


    public function getRandomQuestionForUser(User $user): Question
    {
        $answeredQuestionsIds = $this->getAnsweredQuestionsByUser($user)->fetchPairs(NULL, 'id');

        if (count($answeredQuestionsIds) >= self::MAX_USER_ANSWERS) {
            throw new UserAnsweredOnMaximumQuestions();
        }

        $builder = $this->builder()->orderBy('random()');

        if ($answeredQuestionsIds) {
            $builder = $builder->where('NOT id IN %i[]', $answeredQuestionsIds);
        }

        /** @var NULL|Question $question */
        $question = $this->toEntity($builder);

        if ( ! $question) {
            throw new NoQuestionsAvailableForUser();
        }

        return $question;
    }


    /**
     * @return ICollection<Question>
     */
    private function getAnsweredQuestionsByUser(User $user): ICollection
    {
        $builder = $this->builder()
            ->select('question.id')
            ->from('answer')
            ->innerJoin(
                'answer',
                'question_option',
                'question_option',
                'question_option.id = answer.choosen_option AND answer.user_id = %i',
                $user->getId(),
            )
            ->innerJoin(
                'question_option',
                'question',
                'question',
                'question_option.question_id = question.id',
            )
        ;

        return $this->toCollection($builder);
    }
}
