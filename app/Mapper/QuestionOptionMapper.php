<?php declare(strict_types = 1);

namespace App\Mapper;

use Nextras\Orm\Mapper\Mapper;

final class QuestionOptionMapper extends Mapper
{
    public function getTableName(): string
    {
        return 'question_option';
    }
}
