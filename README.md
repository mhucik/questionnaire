# questionnaire

Simple questionnaire application

## How to start

1. clone this repository 
    ```shell script
    git clone https://gitlab.com/mhucik/questionnaire.git
    ```
2. install frontend and backend dependencies with docker-compose or with your custom composer/node
    ```shell script
    docker-compose run node npm install
    docker-compose run phpserver composer install
    ```
3. build frontend
    ```shell script
    docker-compose run node npm run build
    ```
4. run database migration
    ```
   docker-compose run phpserver php bin/console migration:continue
    ```
5. start application
    ```shell script
    docker-compose up -d
    ```
6. visit http://localhost on your favorite browser

## Application

### Login to Admin and create questions
- on login page you can choose between new registration and login with user code or login to administration
- first you should login to administration and prepare some questions for users
- go to http://localhost/admin and login with credentials set in neon config (defaultly user: `testc, pass `test`)
- then you can add as many questions with options as you want. Dont forget to fill question text input, at least one option input and choose correct answer with click on radio button

### Create new user
- go back to homepage http://localhost/ (you can logout from admin) and click on `Register to questionnaire`. You should see your code in flash message on top of the page
- login with generated code
- answer questions
- questions are generated randomly, every user can answer only on 10 question, more wont be available to answer
- you can find list of your answers click to Answer link in top menu