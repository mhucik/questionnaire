const webpack = require('webpack');
const merge = require("webpack-merge");
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');

const devMode = process.env.NODE_ENV !== "production";
const ROOT_PATH = __dirname;
const CACHE_PATH = ROOT_PATH + "/temp/webpack";
const WEBPACK_DEV_SERVER_HOST = process.env.WEBPACK_DEV_SERVER_HOST || "localhost";
const WEBPACK_DEV_SERVER_PORT = parseInt(process.env.WEBPACK_DEV_SERVER_PORT, 10) || 8888;
const WEBPACK_DEV_SERVER_PROXY_HOST = process.env.WEBPACK_DEV_SERVER_PROXY_HOST || "localhost";
const WEBPACK_DEV_SERVER_PROXY_PORT = parseInt(process.env.WEBPACK_DEV_SERVER_PROXY_PORT, 10) || 80;

module.exports = {
    mode: devMode ? "development" : "production",
    context: path.join(ROOT_PATH, "www/assets"),
    entry: {
        app: path.join(ROOT_PATH, "www/assets/app.js"),
        backend: path.join(ROOT_PATH, "www/assets/backend.js")
    },
    output: {
        path: path.join(ROOT_PATH, 'www/dist'),
        publicPath: "/dist/",
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: file => (
                    /node_modules/.test(file) &&
                    !/\.js/.test(file)
                ),
                use: [
                    ...!devMode ? [] : [
                        {
                            loader: 'cache-loader',
                            options: {
                                cacheDirectory: path.join(CACHE_PATH, "babel-loader"),
                            }
                        },
                        {
                            loader: 'thread-loader',
                            options: {
                                workers: require('os').cpus().length - 1,
                            },
                        },
                    ],
                    ...[{
                        loader: 'babel-loader',
                    }],
                ]
            },
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    ...!devMode ? [] : [
                        {
                            loader: 'cache-loader',
                            options: {
                                cacheDirectory: path.join(CACHE_PATH, "ts-loader"),
                            }
                        },
                    ],
                    ...[{
                        loader: 'awesome-typescript-loader',
                    }],
                ]
            },
            {
                test: /\.(css|scss|sass)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false,
                            importLoaders: 2,
                            modules: false
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            ident: "postcss",
                            plugins: [require("autoprefixer")]
                        }
                    },
                    "sass-loader"
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: !devMode ? "[name].[chunkhash:8].bundle.css" : "[name].bundle.css",
        }),
        new ManifestPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jquery: "jquery",
            "window.jQuery": "jquery",
            jQuery:"jquery"
        }),
        new webpack.ProvidePlugin({
            naja: "naja"
        })
    ],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "www/assets")
        },
        extensions: [".js", ".ts", ".tsx"],
        modules: [
            'node_modules',
        ],
    },
}

if (process.env.NODE_ENV === "development") {b
    const development = {
        output: {
            globalObject: 'this'
        },
        devServer: {
            host: WEBPACK_DEV_SERVER_HOST,
            port: WEBPACK_DEV_SERVER_PORT,
            disableHostCheck: true,
            contentBase: path.join(ROOT_PATH, "www"),
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
            stats: "errors-only",
            hot: true,
            inline: true,
            proxy: {
                "/": `http://${WEBPACK_DEV_SERVER_PROXY_HOST}:${WEBPACK_DEV_SERVER_PROXY_PORT}`
            }
        },
    };

    module.exports = merge(module.exports, development)
}

if (process.env.NODE_ENV === "production") {
    const production = {
        output: {
            filename: '[name].[contenthash:8].bundle.js',
            chunkFilename: '[name].[contenthash:8].chunk.js'
        },
        devtool: "none",
        optimization: {
            minimizer: [
                new TerserPlugin({
                    test: /\.m?js(\?.*)?$/i,
                    warningsFilter: () => true,
                    extractComments: false,
                    sourceMap: true,
                    cache: true,
                    cacheKeys: defaultCacheKeys => defaultCacheKeys,
                    parallel: true,
                    include: undefined,
                    exclude: undefined,
                    minify: undefined,
                    terserOptions: {
                        output: {
                            comments: /^\**!|@preserve|@license|@cc_on/i
                        },
                        compress: {
                            arrows: false,
                            collapse_vars: false,
                            comparisons: false,
                            computed_props: false,
                            hoist_funs: false,
                            hoist_props: false,
                            hoist_vars: false,
                            inline: false,
                            loops: false,
                            negate_iife: false,
                            properties: false,
                            reduce_funcs: false,
                            reduce_vars: false,
                            switches: false,
                            toplevel: false,
                            typeofs: false,
                            booleans: true,
                            if_return: true,
                            sequences: true,
                            unused: true,
                            conditionals: true,
                            dead_code: true,
                            evaluate: true
                        },
                        mangle: {
                            safari10: true
                        }
                    }
                })
            ],
        },
        plugins: [
            new OptimizeCSSAssetsPlugin(),
        ],
    };

    module.exports = merge(module.exports, production)
}