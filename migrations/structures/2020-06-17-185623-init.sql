CREATE SEQUENCE question_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."question" (
    "id" integer DEFAULT nextval('question_id_seq') NOT NULL,
    "question" character varying(255) NOT NULL,
    CONSTRAINT "question_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE SEQUENCE question_option_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."question_option" (
    "id" integer DEFAULT nextval('question_option_id_seq') NOT NULL,
    "option" character varying(255) NOT NULL,
    "correct_option" boolean NOT NULL,
    "question_id" integer NOT NULL,
    CONSTRAINT "question_option_id" PRIMARY KEY ("id"),
    CONSTRAINT "question_option_question_id_fkey" FOREIGN KEY (question_id) REFERENCES question(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "question_option_question_id" ON "public"."question_option" USING btree ("question_id");

CREATE SEQUENCE user_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."user" (
    "id" integer DEFAULT nextval('user_id_seq') NOT NULL,
    "code" character varying(6) NOT NULL,
    CONSTRAINT "user_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE SEQUENCE answer_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."answer" (
    "id" integer DEFAULT nextval('answer_id_seq') NOT NULL,
    "choosen_option" integer NOT NULL,
    "user_id" integer NOT NULL,
    CONSTRAINT "answer_id" PRIMARY KEY ("id"),
    CONSTRAINT "answer_choosen_option_fkey" FOREIGN KEY (choosen_option) REFERENCES question_option(id) ON DELETE CASCADE NOT DEFERRABLE,
    CONSTRAINT "answer_user_id_fkey" FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);
